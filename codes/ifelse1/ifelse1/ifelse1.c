#include <stdio.h>

int main()
{
    int i;

    i = 3;

    if (i <= 2)
    {
        printf("i is less of equal to 2\n");
    }
    else if (i != 5)
    {
        printf("i is greater than 2, not equal to 5\n");
    }
    else
    {
        printf("i is equal to 5\n");
    }
    return 0;
}