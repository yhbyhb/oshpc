#include <stdio.h>

#define N 10000

int main()
{
    double x[N], y[N];
    int i;

    for (i = 0; i < N; i++)
    {
        x[i] = 3.0 * i;
    }

    for (i = 0; i < N; i++)
    {
        y[i] = 2.0 * x[i];
    }

    printf("Last y computed: %f\n", y[N - 1]);
    return 0;
}